function addChildRow_Personal() {
    const Personal = document.getElementById("personal");
    
    const row = document.createElement("div");
    row.setAttribute("class","row row-gap-2 my-1 my-md-0 border-md-top-0 justify-content-between");

    // const firstCol = document.createElement("div");
    // firstCol.setAttribute("class","firstColumn py-2 order-first");
    // firstCol.textContent = "#";
    // row.append(firstCol);
    addIncrementCol(row,"names");
    
    let obj = {
        names : ['Full Name','text'],
        icNos : ['IC. No','text'],
        emails : ['Email Address','email'],
        hpNos : ['Cellphone No.','tel'] 
    }

    for (key in obj) {
        const childDiv = document.createElement("div");
        childDiv.setAttribute("class","col-md")
        
        const input = document.createElement("input");
        input.setAttribute("placeholder",obj[key][0]);
        input.setAttribute("type",obj[key][1]);
        input.setAttribute("class","form-control");
        input.setAttribute("name",key);
        

        childDiv.append(input)
        row.append(childDiv);
    }

    
    // const lastCol = document.createElement("div");
    // lastCol.setAttribute("class","lastColumn order-first order-md-last");
    // lastCol.textContent = "/";
    // row.append(lastCol);
    
    // removeParentButton(tr,sixCols)

    addRemoveColumn(row,"personal")
    Personal.append(row);
}

// dependent to countLengthByName
function addIncrementCol(parent,name){
    
    const firstCol = document.createElement("div");
    firstCol.setAttribute("class","firstColumn py-2 order-first");
    
    let i = countLengthByName(name)+1;

    firstCol.textContent = i+".";
    parent.append(firstCol);
}

function countLengthByName(name){
    return document.getElementsByName(name).length;
}

// this one doesn't work
// function removeParent() {
//     this.parentElement.remove(); 
// }

// listener for add team member row
document.getElementById("personal-addButton")
    .addEventListener("click",()=>{
        addChildRow_Personal();
    });

function addRemoveColumn(parent,id) {
    
    const lastCol = document.createElement("div");
    lastCol.setAttribute("class","lastColumn order-first order-md-last");
    lastCol.textContent = "-";
        
    // let ionIcon = document.createElement("ion-icon");
    // ionIcon.setAttribute("name","remove-circle");
      
    // ionIcon.onclick = function(){    
    //     removeParent();
    //     // incrementRecount(id);
    //     };
    
    lastCol.onclick = function(){    
        this.parentElement.remove();
        incrementRecount(id)
        };
    
    // lastCol.append(ionIcon);
    parent.append(lastCol);
}

function incrementRecount(id) {
    // renumber the first column of incremental number
    // it supposed to renumber after a minus button pressed
    // note :   the hardest part was how to get the first child of a row from a table,
    //          however can always use getElement from the top/parent.
    //          using textContent is kinda bad, maybe should used Id

    let rows = document.getElementById(id).getElementsByClassName("firstColumn");
    let i = 1;
    
    for (row of rows) {
        if (row.textContent === "#") { continue }  
        row.textContent = i+".";
        i++;
    }
}


function addChildRow_Entry(){
    const Entry = document.getElementById("entry");
    
    const row = document.createElement("div");
    row.setAttribute("class","row row-gap-2 my-1 my-md-0 border-md-top-0 justify-content-between");

    addIncrementCol(row,"titles");
    
    let obj = {
        titles : ['Title','text'],
        dates : ['Date','text']
    }

    for (key in obj) {
        const childDiv = document.createElement("div");
        childDiv.setAttribute("class","col-md")
        
        const input = document.createElement("input");
        input.setAttribute("placeholder",obj[key][0]);
        input.setAttribute("type",obj[key][1]);
        input.setAttribute("class","form-control");
        input.setAttribute("name",key);
        if (input.name === "dates") childDiv.className = "col-md-3";

        childDiv.append(input)
        row.append(childDiv);
    }
     
    addRemoveColumn(row,"entry")
    Entry.append(row);
}

// listener for add entry & date row
document.getElementById("entry-addButton")
    .addEventListener("click",()=>{
        addChildRow_Entry();
    });


console.log(`addChildRow_Personal()
addChildRow_Entry()
countLengthByName()
addRemoveColumn(parent,id)`)